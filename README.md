# Ubuntu Provisioning Script
This repository contains a basic bash script to quickly get a Ubuntu machine ready for java development.

## Tools installed
- Java SDK
- Git
- Gradle
- IntelliJ Idea Community Edition
- Docker
- Docker-Compose
- Kubectl

## How To Run
### Choosing What to Install
If you don't one to install one of the tools then simply comment out (by prefacing the line with '#') the "installX" function call at the bottom of the script.
These can be found on lines 132, to 139 under the "main" comment.

### Running the Script
Once you have commented out any stools you don't want to install, simply go to the directory containing the script and run these two commands.

```
chmod 777 provisioning-script
sudo ./provisioning-script
```
